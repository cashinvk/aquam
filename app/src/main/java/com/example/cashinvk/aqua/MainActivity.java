package com.example.cashinvk.aqua;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;
import com.john.waveview.WaveView;

public class MainActivity extends AppCompatActivity {
    private WaveView waveView;
    private ImageView orangeFish;
    private ImageView blueFish;
    private ImageView yellowFish;

    //отвечает за отображение рыбок, если true - плавают
    private boolean statusFish = false;

    //отвечает за заполненность аквариума, если true - аквариум заполнен
    private boolean statusWater = false;

    //анимация передвижения рыбок
    private Animation translateOrangeFish;
    private Animation translateBlueFish;
    private Animation translateYellowFish;

    //анимация прозрачности, масштабирования, поворота
    private Animation alphaAnimation;
    private Animation scaleAnimation;
    private Animation rotateAnimation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        //объект waveView, имитирующий воду
        waveView = (WaveView) findViewById(R.id.waveView);

        //объекты imageView  с картинками рыбок
        orangeFish = (ImageView) findViewById(R.id.imageViewOrangeFish);
        blueFish = (ImageView) findViewById(R.id.imageViewBlueFish);
        yellowFish = (ImageView) findViewById(R.id.imageViewYellowFish);

        //для каждой рыбки загружаем индивидуальную анимацию перемещения
        translateOrangeFish = AnimationUtils.loadAnimation(this, R.anim.transorange);
        translateBlueFish = AnimationUtils.loadAnimation(this, R.anim.transblue);
        translateYellowFish = AnimationUtils.loadAnimation(this, R.anim.transyellow);

        //загружаем анимации alpha, scale, rotate из файлов xml
        alphaAnimation = AnimationUtils.loadAnimation(this, R.anim.alpha);
        scaleAnimation = AnimationUtils.loadAnimation(this, R.anim.scale);
        rotateAnimation = AnimationUtils.loadAnimation(this, R.anim.rotate);

        //изначально в аквариуме не должно быть воды и рыбок
        deleteWater();
        viewFish(0);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
//меню
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.upWater:
                if(statusWater){
                    outError("Аквариум уже полон :)");
                }
                //заполняем аквариума водой
                addWater();
                return true;

            case R.id.downWater:
                if(!statusWater){
                    outError("Аквариум уже пуст :)");
                }
                //убираем воду, а вместе с ней и рыбок
                deleteWater();
                viewFish(0);
                return true;

            case R.id.startFish:
                if(statusFish){
                    outError("Рыбки уже есть :)");
                }
                if(!statusWater){
                    outError("Рыбки не могут плавать в воздухе :)");
                }else
                if (statusWater) {
                    statusFish = true;
                    //отображаем рыбок и заставляем их плавать
                    viewFish(1);
                    startFish();
                }
                return true;

            case R.id.alphaAnimation:
               if (statusWater  && statusFish) {
                    showAnimation(alphaAnimation);
               }
                return true;

            case R.id.scaleAnimation:
                if (statusWater  && statusFish) {
                    showAnimation(scaleAnimation);
                }
                return true;

            case R.id.rotateAnimation:
                if (statusWater  && statusFish) {
                    showAnimation(rotateAnimation);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }}
    //отвечает за вывод сообщений об ошибках
    public void outError(String message){
        Toast toast = Toast.makeText(getApplicationContext(),
                message, Toast.LENGTH_LONG);
        toast.show();
    }

    //добавляет воду в аквариум
    public void addWater(){
        waveView.setProgress(100);
        statusWater = true;
    }

    //убирает воду из аквариума
    public void deleteWater(){
        //убираем воду
        waveView.setProgress(1);
        statusWater = false;
        statusFish = false;
    }

    //отображаем или скрывает рыбок
    public void viewFish(float alpha){
        orangeFish.setAlpha(alpha);
        blueFish.setAlpha(alpha);
        yellowFish.setAlpha(alpha);
    }

    // запуск анимаций передвижений для рыбок
    public void startFish(){
        orangeFish.startAnimation(translateOrangeFish);
        blueFish.startAnimation(translateBlueFish);
        yellowFish.startAnimation(translateYellowFish);
    }

    //применение выбранной анимации к рыбкам
    public void showAnimation(Animation anim){
        orangeFish.startAnimation(anim);
        blueFish.startAnimation(anim);
        yellowFish.startAnimation(anim);
    }
}